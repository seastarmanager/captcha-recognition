From ubuntu:latest

MAINTAINER seastar  317490830@qq.com

RUN apt-get update && apt-get install -y git python3 python3-dev python3-pip wget
RUN git clone https://gitlab.com/seastarmanager/captcha-recognition.git
RUN cd /usr/local/bin && ln -s /usr/bin/python3 python && pip3 install --upgrade pip
RUN cd /captcha-recognition && pip install http://download.pytorch.org/whl/cpu/torch-0.4.1-cp36-cp36m-linux_x86_64.whl && pip install --no-cache-dir --trusted-host mirrors.aliyun.com -i http://mirrors.aliyun.com/pypi/simple/ -r requirement.txt
#wget https://bootstrap.pypa.io/get-pip.py && python get-pip.py

CMD cd /captcha-recognition && \
    export LC_ALL=C.UTF-8 && \
    export LANG=C.UTF-8 && \
    git pull && \
    wget https://buaamc.oss-cn-beijing.aliyuncs.com/captcha-model/v3_1/model.pkl && \
    mv model.pkl ./offline/1/ && \
    /bin/sh -x start.sh