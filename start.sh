#!/bin/bash

mkdir /logs
mkdir /logs/image
mkdir ./cache
ls ./cache | xargs -t -I {} mv ./cache/{} /logs/image
ls ./cache/ | xargs -t -I {} rm -rf ./cache/{}
export FLASK_APP=main.py
flask run --host=0.0.0.0 --port=80