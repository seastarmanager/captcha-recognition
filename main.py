# -*- coding: utf-8 -*-

from flask import Flask, request, jsonify, render_template
from flask import request
from recognition import test,url_predict,generate,feedback

app = Flask(__name__,static_folder='cache')
app.config['MAX_CONTENT_LENGTH'] = 1 * 1024 * 1024 #upload filesize < 1M

@app.route('/')
def index():
    return 'Captcha Recognition'

#captcha-recognition service
captcha_api=[test,url_predict,generate,feedback]
for api in captcha_api:
    api.load(app)

