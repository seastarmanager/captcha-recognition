# -*- coding: UTF-8 -*-
import torch
import torch.nn as nn
import captcha_setting

# CNN Model (类似mobilenet_v1)
class CNN(nn.Module):
    def __init__(self):
        super(CNN, self).__init__()

        def conv_bn(inp, oup, stride):  # 第一层传统的卷积：conv3*3+BN+ReLU
            inp = int(inp)
            oup = int(oup)
            return nn.Sequential(
            nn.Conv2d(inp, oup, 3, stride, 1, bias=False),
            # nn.BatchNorm2d(oup, momentum=0.01),
            nn.GroupNorm(1, oup),
            nn.ReLU(inplace=True))
        def conv_dw(inp, oup, stride):  # 其它层的depthwise convolution：conv3*3+BN+ReLU+conv1*1+BN+ReLU
            inp = int(inp)
            oup = int(oup)
            return nn.Sequential(
            nn.Conv2d(inp, inp, 3, stride, 1, groups=inp, bias=False),
            # nn.BatchNorm2d(inp, momentum=0.01),
            nn.GroupNorm(1, inp),
            nn.ReLU(inplace=True),
            nn.Conv2d(inp, oup, 1, 1, 0, bias=False),
            # nn.BatchNorm2d(oup, momentum=0.01),
            nn.GroupNorm(1, oup),
            nn.ReLU(inplace=True))

        width_rate = 1.0
        r = width_rate
        self.model = nn.Sequential( #60*160*1
            conv_bn(1, 64*r, 2), #30*80*32
            conv_dw(64*r, 64*r, 1), #30*80*64
            conv_dw(64*r, 128*r, 2), #15*40*128
            conv_dw(128*r, 128*r, 1), #15*40*128
            conv_dw(128*r, 256*r, 2), #8*20*256
            conv_dw(256*r, 256*r, 1), #8*20*256
            conv_dw(256*r, 512*r, 2), #4*10*512
            conv_dw(512*r, 512, 1), #4*10*512
            # conv_dw(512, 1024, 2), #2*5*1024
            # conv_dw(1024, 1024, 1), #2*5*1024
            nn.AvgPool2d([4,10]),
        )
        len_dim = captcha_setting.MAX_CAPTCHA - captcha_setting.MIN_CAPTCHA + 1
        self.fc = nn.Sequential( # 全连接层
            nn.Linear(512, len_dim+captcha_setting.MAX_CAPTCHA*captcha_setting.ALL_CHAR_SET_LEN),
        )

    def forward(self, x):
        x = self.model(x)
        x = x.view(-1, 512)
        out = self.fc(x)
        return out

    def save(self, global_step, state_dict, optim_dict, save_path):
        state = {
            'global_step' : global_step,
            'state_dict' : state_dict,
            'optim_dict' : optim_dict
        }
        torch.save(state, save_path)

    def load(self, load_path):
        if torch.cuda.is_available():
            ckpt = torch.load(load_path)
        else:
            ckpt = torch.load(load_path, map_location='cpu')
        return ckpt
