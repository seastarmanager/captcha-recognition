# -*- coding: UTF-8 -*-
import numpy as np
import captcha_setting


def encode(text):
    len_dim = captcha_setting.MAX_CAPTCHA - captcha_setting.MIN_CAPTCHA + 1
    vector = np.zeros(len_dim + captcha_setting.ALL_CHAR_SET_LEN * captcha_setting.MAX_CAPTCHA, dtype=float)
    def char2pos(c):
        if c =='_':
            k = 62
            return k
        k = ord(c)-48  #如果是0-9
        if k > 9:
            k = ord(c) - 55 #如果是A-Z,则编码从10开始
            if k > 35:
                k = ord(c) - 61  #如果是a-z,则编码从36开始
                if k > 61:
                    raise ValueError('error')
        return k
    vector[len(text) - captcha_setting.MIN_CAPTCHA] = 1.0
    for i, c in enumerate(text):
        idx = len_dim + i * captcha_setting.ALL_CHAR_SET_LEN + char2pos(c)
        vector[idx] = 1.0
    return vector

def decode(vec):
    char_pos = vec.nonzero()[0]
    text=[]
    len_dim = captcha_setting.MAX_CAPTCHA - captcha_setting.MIN_CAPTCHA + 1
    pred_len = char_pos[0] + captcha_setting.MIN_CAPTCHA
    for i, c in enumerate(char_pos[1:pred_len+1]):
        char_at_pos = i #c/63
        char_idx = (c-len_dim) % captcha_setting.ALL_CHAR_SET_LEN
        if char_idx < 10:
            char_code = char_idx + ord('0')
        elif char_idx <36:
            char_code = char_idx - 10 + ord('A')
        elif char_idx < 62:
            char_code = char_idx-  36 + ord('a')
        elif char_idx == 62:
            char_code = ord('_')
        else:
            raise ValueError('error')
        text.append(chr(char_code))
    return "".join(text)

if __name__ == '__main__':
    e = encode("XKNK")
    print(e)
    print(e.nonzero())
    print(decode(e))