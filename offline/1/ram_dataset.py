# -*- coding: UTF-8 -*-
import os
from torch.utils.data import DataLoader,Dataset
import torchvision.transforms as transforms
from PIL import Image
import one_hot_encoding as ohe
import captcha_setting
import captcha_gen

class ram_dataset(Dataset):
    def __init__(self, transform=None):
        self.transform = transform

    def __len__(self):
        return 1000000000

    #generate data in RAM memory, not read from disk.
    def __getitem__(self, idx):
        text, image = captcha_gen.gen_captcha_text_and_image()
        if self.transform is not None:
            image = self.transform(image)
        label = ohe.encode(text) # 对label做 one-hot 处理
        return image, label

transform = transforms.Compose([
    # transforms.ColorJitter(),
    transforms.Grayscale(),
    transforms.Resize([captcha_setting.IMAGE_HEIGHT, captcha_setting.IMAGE_WIDTH]),
    transforms.ToTensor(),
    # transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
])

def get_new_data_loader():
    dataset = ram_dataset(transform=transform)
    return DataLoader(dataset, batch_size=64, shuffle=False, num_workers= 3)
