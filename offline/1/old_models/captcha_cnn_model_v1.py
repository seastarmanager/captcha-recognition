# -*- coding: UTF-8 -*-
import torch
import torch.nn as nn
import captcha_setting

# CNN Model (2 conv layer)
class CNN(nn.Module):
    def __init__(self):
        super(CNN, self).__init__()
        self.layer1 = nn.Sequential(
            nn.Conv2d(1, 32, kernel_size=3, padding=1),
            nn.BatchNorm2d(32),
            nn.Dropout(0.5),  # drop 50% of the neuron
            nn.ReLU(),
            nn.MaxPool2d(2))
        self.layer2 = nn.Sequential(
            nn.Conv2d(32, 64, kernel_size=3, padding=1),
            nn.BatchNorm2d(64),
            nn.Dropout(0.5),  # drop 50% of the neuron
            nn.ReLU(),
            nn.MaxPool2d(2))
        self.layer3 = nn.Sequential(
            nn.Conv2d(64, 64, kernel_size=3, padding=1),
            nn.BatchNorm2d(64),
            nn.Dropout(0.5),  # drop 50% of the neuron
            nn.ReLU(),
            nn.MaxPool2d(2))
        self.fc = nn.Sequential(
            nn.Linear((captcha_setting.IMAGE_WIDTH//8)*(captcha_setting.IMAGE_HEIGHT//8)*64, 1024),
            nn.Dropout(0.5),  # drop 50% of the neuron
            nn.ReLU())
        len_dim = captcha_setting.MAX_CAPTCHA - captcha_setting.MIN_CAPTCHA + 1
        self.rfc = nn.Sequential(
            nn.Linear(1024, len_dim+captcha_setting.MAX_CAPTCHA*captcha_setting.ALL_CHAR_SET_LEN),
        )

    def forward(self, x):
        out = self.layer1(x)
        out = self.layer2(out)
        out = self.layer3(out)
        out = out.view(out.size(0), -1)
        out = self.fc(out)
        out = self.rfc(out)
        return out

    def save(self, global_step, state_dict, optim_dict, save_path):
        state = {
            'global_step' : global_step,
            'state_dict' : state_dict,
            'optim_dict' : optim_dict
        }
        torch.save(state, save_path)

    def load(self, load_path):
        if torch.cuda.is_available():
            ckpt = torch.load(load_path)
        else:
            ckpt = torch.load(load_path, map_location='cpu')
        return ckpt
