# -*- coding: UTF-8 -*-
from captcha.image import ImageCaptcha  # pip install captcha
from PIL import Image
import random
import time
import os
import sys
sys.path.append('..')
import captcha_setting


def random_captcha():
    captcha_text = []
    random_len = random.choice([4,4,4,4,4,5,6,7,8,8,])
    #random_len = 8
    for i in range(captcha_setting.MAX_CAPTCHA):
        if i < random_len:
            c = random.choice(captcha_setting.ALL_CHAR_SET+captcha_setting.alphabet) #不区分大小写,但生成的时候大小写都有
            captcha_text.append(c)
        else:
            captcha_text.append('_')
    return ''.join(captcha_text)

# 生成字符对应的验证码
def gen_captcha_text_and_image(spec_text = ''):
    image = ImageCaptcha()
    captcha_text = random_captcha().strip('_') if len(spec_text)==0 else spec_text
    captcha_image = Image.open(image.generate(captcha_text))
    captcha_image = captcha_image.resize(
        (int(min(max(random.normalvariate(captcha_setting.IMAGE_WIDTH, 20), 120), 200)),
         int(min(max(random.normalvariate(captcha_setting.IMAGE_HEIGHT, 10), 40), 80))))
    zoom = random.uniform(0.8, 1.3)
    captcha_image = captcha_image.resize(
        (int(captcha_image.size[0] * zoom),
         int(captcha_image.size[1] * zoom)))
    captcha_text = captcha_text.upper()
    return captcha_text, captcha_image

if __name__ == '__main__':
    count = 1000
    #path = captcha_setting.PREDICT_DATASET_PATH
    #path = captcha_setting.TRAIN_DATASET_PATH
    path = captcha_setting.TEST_DATASET_PATH
    for i in range(count):
        now = str(int(time.time()))
        text, image = gen_captcha_text_and_image()
        filename = text+'_'+now+'.png'
        image.save(path  + os.path.sep +  filename)
        print('saved %d : %s' % (i+1,filename))

