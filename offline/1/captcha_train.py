# -*- coding: UTF-8 -*-
import time
import torch
import torch.nn as nn
from torch.autograd import Variable
import my_dataset,ram_dataset
from captcha_cnn_model import CNN

# Hyper Parameters
num_epochs = 200
# batch_size = 64
learning_rate = 0.001


def main():
    if torch.cuda.is_available():
        cnn = CNN().cuda()
        use_cuda = True
    else:
        cnn = CNN()
        use_cuda = False
    global_step = 0
    try:
        ckpt = cnn.load('model.pkl')
        global_step = ckpt['global_step']
        cnn.load_state_dict(ckpt['state_dict'])
        print('train from last model')
    except:
        print('train from new model')
    cnn.train()
    print('init net')
    #criterion = nn.MultiLabelSoftMarginLoss()
    criterion = nn.BCEWithLogitsLoss()
    optimizer = torch.optim.Adam(cnn.parameters(), lr=learning_rate, weight_decay=0.000000)
    try:
        optimizer.load_state_dict(ckpt['optim_dict'])
        print('load optim_dict')
    except:
        pass

    # Train the Model
    #train_dataloader = my_dataset.get_train_data_loader()
    train_dataloader = ram_dataset.get_new_data_loader() # training will not stop if use this
    ts = time.time()
    for epoch in range(num_epochs):
        for i, (images, labels) in enumerate(train_dataloader):
            if use_cuda:
                images = Variable(images).cuda()
                labels = Variable(labels.float()).cuda()
            else:
                images = Variable(images)
                labels = Variable(labels.float())
            predict_labels = cnn(images)
            loss = criterion(predict_labels, labels)
            # print(loss.item())
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
            global_step += 1
            if (i+1) % 10 == 0:
                speed = '%.2f' % (len(labels) * 10.0 / (time.time() - ts))
                ts = time.time()
                print("epoch:", epoch, "step:", i, 'global_step:', global_step, 'samples/sec:', speed, "loss:", loss.item())
            if (i+1) % 200 == 0:
                cnn.save(global_step, cnn.state_dict(), optimizer.state_dict(), './model.pkl') #current is model.pkl
                print("save model")
                import captcha_test
                captcha_test.main()
        print("epoch:", epoch, "step:", i, 'global_step:', global_step, "loss:", loss.item())
    cnn.save(global_step, cnn.state_dict(), optimizer.state_dict(), './model.pkl')  # current is model.pkl
    print("save last model")

if __name__ == '__main__':
    main()