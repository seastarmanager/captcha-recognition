# -*- coding: UTF-8 -*-
import numpy as np
import torch
from torch.autograd import Variable
import captcha_setting
import my_dataset
from captcha_cnn_model import CNN

def main():
    # if torch.cuda.is_available():
    #     cnn = CNN().cuda()
    # else:
    #     cnn = CNN()
    cnn = CNN()
    ckpt = cnn.load('model.pkl')
    cnn.load_state_dict(ckpt['state_dict'])
    cnn.eval()
    print(ckpt['state_dict'])
    print("load cnn net.")

    predict_dataloader = my_dataset.get_predict_data_loader()

    #vis = Visdom()
    for i, (images, labels) in enumerate(predict_dataloader):
        image = images
        vimage = Variable(image)
        predict_label = cnn(vimage)
        len_dim = captcha_setting.MAX_CAPTCHA - captcha_setting.MIN_CAPTCHA + 1
        c = ''
        clen = np.argmax(predict_label[0, 0:len_dim].data.numpy()) + captcha_setting.MIN_CAPTCHA
        for j in range(clen):
            c += captcha_setting.ALL_CHAR_SET[np.argmax(predict_label[0,
                len_dim + j * captcha_setting.ALL_CHAR_SET_LEN:len_dim + (j+1) * captcha_setting.ALL_CHAR_SET_LEN].data.numpy())]
        print(c)
        criterion = torch.nn.BCEWithLogitsLoss()
        loss = criterion(predict_label, labels.float())
        print(loss)
        #vis.images(image, opts=dict(caption=c))

if __name__ == '__main__':
    main()
