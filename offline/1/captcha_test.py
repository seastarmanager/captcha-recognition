# -*- coding: UTF-8 -*-
import numpy as np
import torch
from torch.autograd import Variable
import captcha_setting
import my_dataset
from captcha_cnn_model import CNN
import one_hot_encoding

def main():
    if torch.cuda.is_available():
        cnn = CNN().cuda()
        use_cuda = True
    else:
        cnn = CNN()
        use_cuda = False
    ckpt = cnn.load('model.pkl')
    cnn.load_state_dict(ckpt['state_dict'])
    cnn.eval()
    print("load cnn net.")

    test_dataloader = my_dataset.get_test_data_loader()

    correct = 0
    total = 0
    for i, (images, labels) in enumerate(test_dataloader):
        if use_cuda:
            image = images.cuda()
            vimage = Variable(image).cuda()
        else:
            image = images
            vimage = Variable(image)
        predict_label = cnn(vimage)
        for k in range(len(labels)):
            len_dim = captcha_setting.MAX_CAPTCHA - captcha_setting.MIN_CAPTCHA + 1
            c = ''
            # if use_cuda:
            #     clen = np.argmax(predict_label[0, 0:len_dim].data.numpy()) + captcha_setting.MIN_CAPTCHA
            # else:
            clen = np.argmax(predict_label[k, 0:len_dim].data.cpu().numpy()) + captcha_setting.MIN_CAPTCHA
            for j in range(clen):
                c += captcha_setting.ALL_CHAR_SET[np.argmax(predict_label[k,
                    len_dim + j * captcha_setting.ALL_CHAR_SET_LEN:len_dim + (j+1) * captcha_setting.ALL_CHAR_SET_LEN].data.cpu().numpy())]
            true_label = one_hot_encoding.decode(labels.numpy()[k])
            total += 1#labels.size(0)
            if(c == true_label):
                correct += 1
            if(total%2000==0):
                print('Test Accuracy of the model on the %d test images: %f %%' % (total, 100 * correct / total))
    criterion = torch.nn.BCEWithLogitsLoss()
    if use_cuda:
        loss = criterion(predict_label, labels.cuda().float())
    else:
        loss = criterion(predict_label, labels.float())
    print('Last batch test loss :', loss.item())
    print('Test Accuracy of the model on the %d test images: %f %%' % (total, 100 * correct / total))

if __name__ == '__main__':
    main()
