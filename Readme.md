Deploy with docker:
1. Download Dockerfile to your machine.
2. cd to that dir
3. docker build -t captcha .
4. docker run -d -p 8001:80 -v /logs/captcha:/logs --name captcha-recognize captcha

You can refer to the command in Dockerfile to setup the environment and run the code.

Demo:
http://test.seastar.me:8001/captcha/test
