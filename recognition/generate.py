# -*- coding: utf-8 -*-

import os
import sys
import time
sys.path.append('./offline/1')
import captcha_gen


def load(app):
    @app.route('/captcha/generate/<captcha_text>', methods=['POST','GET'])
    def generate(captcha_text):
        now = str(int(time.time()))
        text, image = captcha_gen.gen_captcha_text_and_image(captcha_text)
        filename = text+'_'+now+'.png'
        image.save(os.path.join('./cache/', filename))
        return captcha_text + '<img src="/cache/'+filename+'" />'
