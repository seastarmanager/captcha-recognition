# -*- coding: utf-8 -*-

import os
import sys
import time


def load(app):
    @app.route('/captcha/feedback/<filename>', methods=['POST','GET'])
    def feedback(filename):
        with open('/logs/feedback.log', 'a') as f:
            f.write('\t'.join([str(time.time()), filename]) + '\n')
        return '[success] feedback ' + filename
