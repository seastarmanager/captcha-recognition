# -*- coding: utf-8 -*-

import os
import sys
from flask import request
import uuid
from urllib.request import urlretrieve
from torchvision import transforms
from PIL import Image
from torch.autograd import Variable
sys.path.append('./offline/1')
from captcha_cnn_model import CNN
import torch
import captcha_setting
import numpy as np
import recognition.test

cnn = CNN()
ckpt = cnn.load('./offline/1/model.pkl')
cnn.load_state_dict(ckpt['state_dict'])
cnn.eval()

def load(app):
    @app.route('/captcha/predict', methods=['POST'])
    def predict_url():
        reqID = str(uuid.uuid1())
        img_url = request.form['img_url']
        ext = '.'+img_url.split('.')[-1]
        def callback(finish,bsize,size):
            if size>1*1024*1024:
                raise Exception('filesize too large')
        try:
            urlretrieve(img_url,'./cache/'+reqID+ext, callback)
            result = recognition.test.predict_img(reqID+ext)
        except:
            result = '[error] maybe file size too large'
        #os.remove('./cache/'+reqID+ext)
        #return 'reqID is' + reqID + '\nimage url is '+img_url
        return result# + '<img src="./cache/'+reqID+ext+'" />'
