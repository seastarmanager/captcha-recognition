# -*- coding: utf-8 -*-

import os
import sys
import time
from flask import Flask, flash, request, redirect, url_for
import uuid
from urllib.request import urlretrieve
from torchvision import transforms
from PIL import Image
from torch.autograd import Variable
sys.path.append('./offline/1')
from captcha_cnn_model import CNN
import torch
import captcha_setting
import numpy as np
from werkzeug.utils import secure_filename

cnn = CNN()
ckpt = cnn.load('./offline/1/model.pkl')
cnn.load_state_dict(ckpt['state_dict'])
cnn.eval()

def allowed_file(filename):
    ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif'])
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

def predict_img(filename):
    transform = transforms.Compose([
        transforms.Grayscale(),
        transforms.Resize([captcha_setting.IMAGE_HEIGHT, captcha_setting.IMAGE_WIDTH]),
        transforms.ToTensor(),
    ])
    img = Image.open('./cache/' + filename)
    image = transform(img).reshape([-1, 1, captcha_setting.IMAGE_HEIGHT, captcha_setting.IMAGE_WIDTH])
    vimage = Variable(image)
    predict_label = cnn(vimage)
    len_dim = captcha_setting.MAX_CAPTCHA - captcha_setting.MIN_CAPTCHA + 1
    c = ''
    clen = np.argmax(predict_label[0, 0:len_dim].data.numpy()) + captcha_setting.MIN_CAPTCHA
    for j in range(clen):
        c += captcha_setting.ALL_CHAR_SET[np.argmax(predict_label[0,
            len_dim + j * captcha_setting.ALL_CHAR_SET_LEN:len_dim + (
            j + 1) * captcha_setting.ALL_CHAR_SET_LEN].data.numpy())]
    with open('/logs/log.log','a') as f:
        f.write('\t'.join([str(time.time()),filename,c])  +'\n')
    return c

resp_head = ''' <!doctype html>
                <title>Recognize Captcha Image</title>
                <h1>Upload Captcha Image</h1>'''
resp_upload = '''
                <form method=post enctype=multipart/form-data>
                    <input type=file name=file>
                    <input type=submit value=Predict>
                </form>
                <br>OR,
                <br>post http://test.seastar.me:8001/captcha/predict
                <br>Body:
                <br> Key: img_url
                <br> Value: http://your.image.url
                <br>
                <br>Note:
                <br>1. Only support jpg,png
                <br>2. Support 4~8 characters in 0~9,A~Z,a~z.
                <br>3. Lowercase letters are replaced by uppercase in the result.
                <br>
                <br>Generate captcha image here:
                <form>
                    <input type=text name=CaptchaText value="123aBcDe">
                    <input type=button value=Generate onclick="open_generate()">
                </form>
                <script>
                    function open_generate(){
                        var captcha = document.getElementsByName("CaptchaText")[0].value;
                        window.open('/captcha/generate/'+captcha)
                    }
                    function feed_back(){
                        var current_image = document.getElementsByName("CurrentImage")[0].src.split('/').pop();
                        var xmlhttp=new XMLHttpRequest();
                        xmlhttp.open("GET","/captcha/feedback/"+current_image,false);
	                    xmlhttp.send();
                        alert('Thank you for your feedback!')
                    }
                </script>
                '''

def load(app):
    @app.route('/captcha/test', methods=['GET', 'POST'])
    def upload_file():
        # file upload
        if request.method == 'POST':
            # check if the post request has the file part
            if 'file' not in request.files:
                #flash('No file part')
                return resp_head + 'No file part' + resp_upload
            file = request.files['file']
            # if user does not select file, browser also
            # submit an empty part without filename
            if file.filename == '':
                #flash('No selected file')
                return resp_head + 'No selected file' + resp_upload
            if file and allowed_file(file.filename):
                filename = secure_filename(file.filename)
                reqID = str(uuid.uuid1())
                ext = '.' + filename.split('.')[-1]
                file.save(os.path.join('./cache/', reqID+ext))
                result = predict_img(reqID+ext)
                return resp_head + \
                '<strong>' + result + '</strong>' +\
                '<img name=CurrentImage src="/cache/'+reqID+ext+'" />' + \
                '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' \
                'Not correct? You can <input type=button value="feedback" onclick="feed_back()">' + \
                resp_upload
        return resp_head + resp_upload


